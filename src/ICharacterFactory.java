
public interface ICharacterFactory {
    public Character makeCharacter();
}
