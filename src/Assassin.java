
public class Assassin extends Character {

    public Assassin() {
        this.name = "Assassin";
        this.actions = new IAction[]{new Fight()};
    }

}
