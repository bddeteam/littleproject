
public class PriestFactory implements ICharacterFactory {
    @Override
    public Priest makeCharacter() {
        return new Priest();
    }
}