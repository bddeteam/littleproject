
public abstract class Character {

    protected String name;
    protected IAction[] actions;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IAction[] getActions() {
        return actions;
    }

    public void setActions(IAction[] actions) {
        this.actions = actions;
    }

    public Character clone() {
        Character character = null;
        character.setName(getName());
        character.setActions(getActions());
        return character;
    }

}
