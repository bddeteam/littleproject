import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Batyrova Elina
 * 11-602
 */
public class Player {

    ArrayList<Character> characters = new ArrayList<>();

    public void addCharacter(String name) {
        ICharacterFactory factory = null;
        if (name.equals("Assassin")) factory = new AssassinFactory();
        if (name.equals("Priest")) factory = new PriestFactory();
        characters.add(factory.makeCharacter());
    }

    public void listOfCharacters() {
        Iterator<Character> iterator = characters.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next().getName());
        }
    }
}
