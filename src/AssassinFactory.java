
public class AssassinFactory implements ICharacterFactory {
    @Override
    public Assassin makeCharacter() {
        return new Assassin();
    }
}
