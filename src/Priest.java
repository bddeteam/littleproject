
public class Priest extends Character{

    public Priest() {
        this.name = "Priest";
        this.actions = new IAction[]{new Fight(), new Hill()};
    }

}